// AJAX виконує мережні запити на сервер і може підвантажувати не всю 
// сторінку одночасно, а лише нову інформацію за необхідності. Це зручно тому
// що непотрібно довго чекати оновлення та завантаження сторінки, 
// економиться трафік

const films = fetch("https://ajax.test-danit.com/api/swapi/films");
films.then(res => res.json())
.then(data => {
    getInfo(data);
    return data
})
.catch(err => console.log(err))

function getInfo(data) {
    data.forEach(({characters, episodeId, name, openingCrawl}) => {
        const wrapper = document.createElement("div")
        wrapper.innerHTML = `
        <h1>Episode ${episodeId}: ${name}</h1>
        <ul class="characters-list"></ul>
        <div class="loader"></div>
        <p>${openingCrawl}</p>
      `
    document.body.append(wrapper);

    const charactersList = wrapper.querySelector('.characters-list');
    const loader = document.querySelectorAll(".loader")

    Promise.all(characters.map(url => fetch(url) 
      .then(response => response.json())))
      .then(characters => {
        charactersList.innerHTML = characters.map(character => 
            `<li>${character.name}</li>`).join('');
            loader.forEach(load => load.remove())
      })
      .catch(error => {console.error(error)});
      })
}


